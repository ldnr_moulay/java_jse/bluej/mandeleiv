
/**
 * Décrivez votre classe Mandeleiv ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class Mandeleiv
{
    // variables d'instance -
    String name, symbol, date;
    
    String[] tab_elem = {"Hydrogène","Hélium","Lithium","Béryllium","Bore","Carbone","Azote","Oxygène","Fluor","Néon","Sodium","Magnésium","Aluminium","Silicium","Phosphore","Soufre","Chlore","Argon","Potassium","Calcium","Scandium","Titane","Vanadium","Chrome","Manganèse","Fer","Cobalt","Nickel","Cuivre","Zinc","Gallium","Germanium","Arsenic","Sélénium","Brome","Krypton","Rubidium","Strontium","Yttrium","Zirconium","Niobium","Molybdène","Technétium","Ruthénium","Rhodium","Palladium","Argent","Cadmium","Indium","Étain","Antimoine","Tellure","Iode","Xénon","Césium","Baryum","Lanthane","Cérium","Praséodyme","Néodyme","Prométhium","Samarium","Europium","Gadolinium","Terbium","Dysprosium","Holmium","Erbium","Thulium","Ytterbium","Lutécium","Hafnium","Tantale","Tungstène","Rhénium","Osmium","Iridium","Platine","Or","Mercure","Thallium","Plomb","Bismuth","Polonium","Astate","Radon","Francium","Radium","Actinium","Thorium","Protactinium","Uranium","Neptunium","Plutonium","Américium","Curium","Berkélium","Californium","Einsteinium","Fermium","Mendélévium","Nobélium","Lawrencium","Rutherfordium","Dubnium","Seaborgium","Bohrium","Hassium","Meitnérium","Darmstadtium","Roentgenium","Copernicium","Nihonium","Flérovium","Moscovium","Livermorium","Tennesse","Oganesson"};   
    String[] tab_symbol = {"H","He","Li","Be","B","C","N","O","F","Ne","Na","Mg","Al","Si","P","S","Cl","Ar","K","Ca","Sc","Ti","V","Cr","Mn","Fe","Co","Ni","Cu","Zn","Ga","Ge","As","Se","Br","Kr","Rb","Sr","Y","Zr","Nb","Mo","Tc","Ru","Rh","Pd","Ag","Cd","In","Sn","Sb","Te","I","Xe","Cs","Ba","La","Ce","Pr","Nd","Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu","Hf","Ta","W","Re","Os","Ir","Pt","Au","Hg","Tl","Pb","Bi","Po","At","Rn","Fr","Ra","Ac","Th","Pa","U","Np","Pu","Am","Cm","Bk","Cf","Es","Fm","Md","No","Lr","Rf","Db","Sg","Bh","Hs","Mt","Ds","Rg","Cn","Nh","Fl","Mc","Lv","Ts","Og"};
    String[] tab_date = {"1766","1895","1817","1797","1808","inconnue","1772","1774","1886","1898","1807","1755","1825","1824","1669","inconnue","1774","1894","1807","1808","1879","1791","1801","1797","1774","inconnue","1735","1751","inconnue","inconnue","1875","1886","v. 1250","1817","1826","1898","1861","1790","1794","1789","1801","1778","1937","1844","1803","1803","inconnue","1817","1863","inconnue","inconnue","1782","1811","1898","1860","1808","1839","1803","1895","1895","1945","1879","1901","1880","1843","1886","1879","1842","1879","1878","1907","1923","1802","1783","1925","1803","1803","1557","inconnue","inconnue","1861","inconnue","1540","1898","1940","1900","1939","1898","1899","1829","1917","1789","1940","1940","1944","1944","1949","1950","1952","1952","1955","1958/66","1961/71","1964/69","1967/70","1974","1976/81","1984","1982","1994","1994","1996","2004","2004","2010","2004","2010","2006"};

    
    //Constructor
    
    public Mandeleiv(int num){
        this.name = tab_elem[num-1];
        this.symbol = tab_symbol[num-1];
        this.date = tab_date[num-1];
    }
    
    

}
